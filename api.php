<?php

  // Basic connection information pertinent to my online MySQL database
  $servername = "localhost";

  // The following user and password combination should NEVER happen. That is,
  // your password should be something unique to you, the individual
  $username = "root";
  $password = "admin";

  // MySQLi is the modern choice for establishing a MySQL connection in PHP
  $conn = new mysqli($servername, $username, $password);

  // Ensure the connection has been established properly, if not, alert the user
  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
  }

  // Function called when specific parameters are met, specifying the request for all contacts.
  function get_contacts($conn)
  {
    $contacts = $conn->query("SELECT * FROM sprout.contacts");

    $myArray = array();

    while($row = $contacts->fetch_array(MYSQL_ASSOC)) {
               $myArray[] = $row;
       }

    return $myArray;
  }

  // Function called when a new contact is to be created.

  function post_contact($conn)
  {
    // There is likely a better way to handle data passed to this particular script
    // This method from memory works, but is not best practice.

    $postdata = file_get_contents("php://input");
    $request = json_decode($postdata);

    $conn->query(sprintf(
      "INSERT INTO sprout.contacts (name, phone, address) VALUES ('%s', '%s', '%s')",
      $conn->real_escape_string($request->name),
      $conn->real_escape_string($request->phone),
      $conn->real_escape_string($request->address)
    ));

  }

  // This is the function called when a user is trying to update a contact. There exists a vulnerability
  // and that is the primary key being modifyable by the user and thus changing any contact.
  // This can be rectified by using pseudo-random PKs or by having a layer of authentication applied

  function update_contact($conn)
  {
    $postdata = file_get_contents("php://input");
    $request = json_decode($postdata);

    $conn->query(sprintf(
      "UPDATE sprout.contacts SET name = '%s', phone = '%s', address = '%s' WHERE id = %d",
      $conn->real_escape_string($request->name),
      $conn->real_escape_string($request->phone),
      $conn->real_escape_string($request->address),
      $conn->real_escape_string($request->id)
    ));

  }

  // Same vulnerability as above exists.
  // Function used to delete a contact once the user has requested it.

  function delete_contact($conn, $id)
  {
    $conn->query("DELETE FROM sprout.contacts WHERE id = " . $id);
  }

  $possible_endpoints = array("get_contacts", "post_contact", "update_contact", "delete_contact");

  if (isset($_GET["action"]) && in_array($_GET["action"], $possible_endpoints))
  {
    switch ($_GET["action"])
      {
        case "get_contacts":
          $return_value = get_contacts($conn);

          break;

        case "post_contact":
            post_contact($conn);

            if ($conn->error)
              $return_value = $conn->error;
            else
              $return_value = "Contact created successfully.";

          break;

        case "update_contact":
          update_contact($conn);

          if ($conn->error)
             $return_value = $conn->error;
           else
            $return_value = "Contact updated successfully.";

          break;

        case "delete_contact":
          if (isset($_GET["id"]))
          {
            delete_contact($conn, $_GET["id"]);

            if (!$conn->error)
              $return_value = "Contact deleted successfully.";
          }

          break;
      }
  }

  // Return the API result
  exit(json_encode($return_value));

?>
