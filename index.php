<!DOCTYPE html>
<html>
  <head>
    <!--

      Generally, I would refrain from making a comment such as this in my HTML.

      I used AngularJS instead of JQuery for this task, but the same can easily be achieved
      in JQuery - .GET and the associated REST methods function nearly identically; the only
      marked difference would be binding results to specific elements.

      As far as I know, JQuery requires the body of any given element to be explicity modified.

    -->
    <title>Sprout Test - John-Curtis McDonald</title>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-MfvZlkHCEqatNoGiOXveE8FIwMzZg4W85qfrfIFBfYc= sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">
  </head>

  <body ng-app="Sprout">

    <div class="container" ng-controller="ContactsController" ng-init="getContacts();">
        <div class="header clearfix">
          <h3 class="text-muted">Sprout Test</h3>
        </div>

        <div class="col-md-6">
          <form>
            <div class="form-group">
              <label for="name">Full Name</label>
              <input type="text" class="form-control" id="contactName" placeholder="Full Name" ng-model="selectedContact.name">
            </div>

            <div class="form-group">
              <label for="phone">Phone Number</label>
              <input type="text" class="form-control" id="contactPhone" placeholder="Phone Number" ng-model="selectedContact.phone">
            </div>

            <div class="form-group">
              <label for="address">Address</label>
              <input type="text" class="form-control" id="contactAddress" placeholder="Address" ng-model="selectedContact.address">
            </div>

            <a ng-if="selectedContact.id" ng-click="updateContact()" class="btn btn-default">Submit Changes</a>
            <a ng-if="selectedContact.id" class="btn btn-primary" ng-click="clearContact()">New Contact</a>
            <a ng-if="!selectedContact.id" ng-click="postContact()" class="btn btn-default">Submit</a>
          </form>
        </div>

        <div class="col-md-6">
          <p>Adding a contact is easy! Simply specify, in full, the <strong>Full Name</strong>, <strong>Phone Number</strong>, and <strong>Address</strong> of the individual in question.</p>

          <p>Please be aware that any changes to make will be <strong>immediately</strong> applied across our database systems.</p>
        </div>

        <div ng-if="result" class="container col-md-12 alert" role="alert" ng-bind="result"></div>

        <table class="table table-hover">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Phone Number</th>
                  <th>Address</th>
                  <th>Configure</th>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="contact in contacts">
                  <th scope="row">{{ contact.id }}</th>
                  <td>{{ contact.name }}</td>
                  <td>{{ contact.phone }}</td>
                  <td>{{ contact.address }}</td>
                  <td>
                    <a class="btn btn-primary" ng-click="selectContact(contact)">Edit</a>
                    <a class="btn btn-danger" ng-click="deleteContact(contact.id)">Delete</a>
                  </td>
                </tr>
              </tbody>
          </table>

      </div>

    <script src="/vendor/angular.min.js" type="text/javascript"></script>
    <script src="/js/app.js" type="text/javascript"></script>
  </body>
</html>
