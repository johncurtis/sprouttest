angular.module('Sprout',
		[
			'ContactsController',
		]
	)

// Would have a dedicated controllers folder in any project larger than this.
// This would help ensure the maintainability of all JavaScript by making it modularized

angular.module('ContactsController', []).controller('ContactsController', function($scope, $rootScope, $location, $http) {

		$scope.selectedContact = {
			name: "",
			phone: "",
			address: ""
		};

		function checkValidContact() {
			if ($scope.selectedContact.name.length > 0 && $scope.selectedContact.phone.length > 0 && $scope.selectedContact.address.length > 0)
			{
					return true;
			}
		}

    $scope.getContacts = function() {

        $http.get('/api.php?action=get_contacts')
            .success(function(response) {
								$scope.contacts = [];
                $scope.contacts = response;
						})
    };

		$scope.deleteContact = function(id) {
			$http.delete('/api.php?action=delete_contact&id=' + id)
				.success(function(response) {
						$scope.getContacts();
						$scope.result = response;
				})
		}

		$scope.postContact = function() {
			if (checkValidContact())
			{
				$http(
								{
									method: 'post',
									url: '/api.php?action=post_contact',
									data: {
										name: $scope.selectedContact.name,
										phone: $scope.selectedContact.phone,
										address: $scope.selectedContact.address
									}
								}
							)
							.success(function(result) {
									$scope.getContacts();
									$scope.selectedContact = {};
									$scope.result = result;
							})
			} else { $scope.result = "Please fill in all required fields!"; }
		}

		// There's a bad vulnerability present with this.
		// Ideally, we wouldn't use or expose sequential primary keys like this.

		$scope.updateContact = function(id) {
			if (checkValidContact())
			{
				$http(
								{
									method: 'post',
									url: '/api.php?action=update_contact',
									data: {
										id: $scope.selectedContact.id,
										name: $scope.selectedContact.name,
										phone: $scope.selectedContact.phone,
										address: $scope.selectedContact.address
									}
								}
							)
							.success(function(result) {
									$scope.getContacts();
									$scope.result = result;
							})
				} else { $scope.result = "Please fill in all required fields!"; }
			}

		$scope.selectContact = function(contact) {
			$scope.selectedContact = contact;
		}

		$scope.clearContact = function() {
			$scope.selectedContact = {};
		}

	});
