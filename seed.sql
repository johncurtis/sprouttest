INSERT INTO sprout.contacts (name, phone, address)
VALUES ('Cody Kretz', '123-456-7890', 'Thorold, Ontario');

INSERT INTO sprout.contacts (name, phone, address)
VALUES ('John-Curtis McDonald', '289-821-4784', 'Oakville, Ontario');

INSERT INTO sprout.contacts (name, phone, address)
VALUES ('Matt Smith', '123-456-7890', 'London, England');

INSERT INTO sprout.contacts (name, phone, address)
VALUES ('David Tennant', '012-345-5111', 'The Tardis');

SELECT * FROM sprout.contacts 